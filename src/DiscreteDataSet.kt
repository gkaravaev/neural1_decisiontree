import kotlin.math.log2

class Example(val attrs: Map<String, String>) {
    fun removeAttr(attribute: Attribute): Example {
        return Example(attrs.filter { attr -> attr.key != attribute.name })
    }
}

class EvaluatedAttribute(val name: String, val value: String)

class Attribute(val name: String, val values: MutableSet<String>)

class DataSet(private val dataLines: List<Example>, val attrs: List<Attribute>) {

    fun getSize(): Int {
        return dataLines.size
    }

    fun allExampleFromOneClass(targetVariable: Attribute): Boolean {
        val firstValue = dataLines[0].attrs[targetVariable.name]
        return dataLines.all { example -> example.attrs[targetVariable.name] == firstValue }
    }

    fun getSolution(targetVariable: Attribute): EvaluatedAttribute {
        return EvaluatedAttribute(targetVariable.name, dataLines[0].attrs.getOrDefault(targetVariable.name, "error!"))
    }

    private fun probability(clj: Pair<String, String>): Float {
        val all = dataLines.map { line -> line.attrs[clj.first] }
        return all.count { it == clj.second } / all.size.toFloat()
    }

    private fun entropy(targetVariable: Attribute): Float {
        return -1.0f * targetVariable.values.fold(0.0f, { acc, value ->
            acc + probability(Pair(targetVariable.name, value)) * log2(probability(Pair(targetVariable.name, value)))
        })
    }

    private fun attrEntropy(targetVariable: Attribute, usedAttribute: Attribute): Float {
        val fragmentation = dataLines.groupBy { line -> line.attrs.getOrDefault(usedAttribute.name, "null") }
        return fragmentation.entries.fold(0.0f, { acc, entry ->
            val dataPart = DataSet(entry.value, attrs)
            acc + entry.value.size.toFloat() / dataLines.size.toFloat() * dataPart.entropy(targetVariable)
        })
    }


    private fun gain(targetVariable: Attribute, usedAttribute: Attribute): Float {
        return entropy(targetVariable) - attrEntropy(targetVariable, usedAttribute)
    }

    private fun splitInfo(fragmentation: Map<String, List<Example>>): Float {
        return -1.0f * fragmentation.entries.fold(0.0f, { acc, entry ->
            val p = entry.value.size / dataLines.size.toFloat()
            acc + p * log2(p)
        })
    }

    fun gainRatio(targetVariable: Attribute, usedAttribute: Attribute): Float {
        return gain(targetVariable, usedAttribute) / splitInfo(fragByAttr(usedAttribute))
    }

    fun maxGainRatio(targetVariable: Attribute): Attribute? {
        return attrs.maxBy { gainRatio(targetVariable, it) }
    }

    fun fragByAttr(attribute: Attribute): Map<String, List<Example>> {
        val fragmentation = dataLines.groupBy { line -> line.attrs.getOrDefault(attribute.name, "null") }
        return fragmentation.mapValues { entry -> entry.value.map { example -> example.removeAttr(attribute) } }
    }
}

