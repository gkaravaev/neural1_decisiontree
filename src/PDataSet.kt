import kotlin.math.log2

// добавить класс из пары последних атрибутов
class PDataSet<T, K>(
    private val dataLines: List<Pair<RawPE<T>, RawPE<K>>>,
    val attrs: List<PAttribute<T>>,
    val targetVar: PAttribute<K>
) {

    class PAttribute<K>(val name: String, val values: MutableSet<K>)

    class RawPE<K>(val attrs: Map<String, K>) {
        fun removeAttr(attribute: PAttribute<K>): RawPE<K> {
            return RawPE(attrs.filter { attr -> attr.key != attribute.name })
        }
    }

    private fun classProbability(clj: Pair<String, K>): Float {
        val all = dataLines.map { line -> line.second.attrs[clj.first] }
        return all.count { it == clj.second } / all.size.toFloat()
    }

    private fun classEntropy(targetVariable: PAttribute<K>): Float {
        return -1.0f * targetVariable.values.fold(0.0f, { acc, value ->
            if (classProbability(Pair(targetVariable.name, value)) == 0.0f)
                acc
            else
                acc + classProbability(
                    Pair(
                        targetVariable.name,
                        value
                    )
                ) * log2(classProbability(Pair(targetVariable.name, value)))
        })
    }

    private fun attrEntropy(targetVariable: PAttribute<K>, usedAttribute: PAttribute<T>): Float {
        val fragmentation = dataLines.groupBy { line -> line.first.attrs[usedAttribute.name] }
        return fragmentation.entries.fold(0.0f, { acc, entry ->
            val dataPart = PDataSet(entry.value, attrs, targetVar)
            acc + entry.value.size.toFloat() / dataLines.size.toFloat() * dataPart.classEntropy(targetVariable)
        })
    }


    fun gain(targetVariable: PAttribute<K>, usedAttribute: PAttribute<T>): Float {
        return classEntropy(targetVariable) - attrEntropy(targetVariable, usedAttribute)
    }

    private fun splitInfo(fragmentation: Map<T?, List<Pair<RawPE<T>, RawPE<K>>>>): Float {
        return -1.0f * fragmentation.entries.fold(0.0f, { acc, entry ->
            val p = entry.value.size / dataLines.size.toFloat()
            acc + p * log2(p)
        })
    }

    fun fragByAttr(attribute: PAttribute<T>): Map<T?, List<Pair<RawPE<T>, RawPE<K>>>> {
        val fragmentation = dataLines.groupBy { line -> line.first.attrs[attribute.name] }
        return fragmentation.mapValues { entry ->
            entry.value.map { example ->
                Pair(
                    example.first.removeAttr(attribute),
                    example.second
                )
            }
        }
    }

    fun gainRatio(targetVariable: PAttribute<K>, usedAttribute: PAttribute<T>): Float {
        return gain(targetVariable, usedAttribute) / splitInfo(fragByAttr(usedAttribute))
    }
}

