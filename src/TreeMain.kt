import java.io.File

fun main(){
    val file = File("data.txt")
    val lines = file.readLines()
    val firstLine = lines[0].split("\\s+".toRegex())
    val stringExamples = lines.subList(1, lines.size).map { line -> line.split("\\s+".toRegex()) }
    val attributes = firstLine.map { line -> Attribute(line, mutableSetOf()) }
    val examples = stringExamples.map{line ->
         Example(line.mapIndexed {i, value ->
             attributes[i].values.add(value)
             Pair(attributes[i].name, value)
         }.toMap())
    }
    val treeNode = TreeNode(EvaluatedAttribute("root", "root"),
        DataSet(examples, attributes),
        mutableListOf(),
        attributes.last()
        )
    treeNode.buildTree()
    treeNode.printTree(emptyList(), treeNode.set.getSize())
}