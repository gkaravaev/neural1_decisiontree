class TreeNode(
    val nodeFragAttribute: EvaluatedAttribute,
    val set: DataSet,
    private val children: MutableList<TreeNode>,
    private val targetVariable: Attribute,
    var isLeaf: Boolean = false
) {
    fun buildTree() {
        if (set.getSize() == 0 || set.allExampleFromOneClass(targetVariable)) {
            isLeaf = true;
            return
        }
        val newFragAttribute = set.maxGainRatio(targetVariable) ?: return
        val newFrag = set.fragByAttr(newFragAttribute)
        val attrList = set.attrs.filter { it.name != newFragAttribute.name }
        newFrag.forEach { fragPiece ->
            val newNode = TreeNode(
                EvaluatedAttribute(newFragAttribute.name, fragPiece.key),
                DataSet(fragPiece.value, attrList),
                mutableListOf(),
                targetVariable
            )
            children.add(newNode)
        }
        children.forEach{child -> child.buildTree()}
    }

    fun getSolution(): EvaluatedAttribute? {
        if (!isLeaf || (isLeaf && set.getSize() == 0)) {
            return null
        } else {
            return set.getSolution(targetVariable)
        }
    }

    fun printTree(attrs: List<EvaluatedAttribute>, firstSize : Int) {
        if (isLeaf) {
            val solution = getSolution()
            val list = attrs + nodeFragAttribute + solution
            list.forEach { print(it?.name + ": " + it?.value + "; ") }
            print(((this.set.getSize().toFloat() / firstSize.toFloat()) * 100).toString() + "%")
            println()
        } else {
            children.forEach{ it -> it.printTree(attrs + nodeFragAttribute, firstSize)}
        }
    }
}
