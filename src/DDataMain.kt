import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

fun main() {
    val file = BufferedReader(InputStreamReader(File("data.csv").inputStream(), "windows-1251"))
    val strings = file.readLines().map { line -> line.split(",") }
    val attrNames = strings[0].subList(0, strings[0].size - 3)
    val attrVals = attrNames.map { mutableSetOf<Float?>() }
    // склеить последние две строки
    val correctStrings = strings.subList(2, strings.size).map { string ->
        val kgf2 = string[string.size - 1].toFloatOrNull()
        val kgf1 = string[string.size - 2].toFloatOrNull()
        if (kgf2 != null) {
            string.subList(0, string.size - 2) + listOf((kgf2 * 1000).toString())
        } else {
            if (kgf1 != null) {
                string.subList(0, string.size)
            } else {
                string.subList(0, string.size - 2) + listOf("")
            }
        }
    }
    // наклепать примеров
    val dataList = mutableListOf<Pair<PDataSet.RawPE<Float?>, PDataSet.RawPE<Pair<Float?, Float?>>>>()
    val classVals = mutableSetOf<Pair<Float?, Float?>>()
    correctStrings.forEach { string ->
        val map = mutableMapOf<String, Float?>()
        for (i in attrNames.indices) {
            attrVals[i].add(string[i].toFloatOrNull())
            map.put(attrNames[i], string[i].toFloatOrNull())
        }
        classVals.add(
            Pair(string[attrNames.size - 2].toFloatOrNull(), string[attrNames.size - 1].toFloatOrNull())
        )
        dataList.add(
            Pair(
                PDataSet.RawPE(map),
                PDataSet.RawPE(
                    mapOf(
                        "class" to
                                Pair(
                                    string[attrNames.size - 2].toFloatOrNull(),
                                    string[attrNames.size - 1].toFloatOrNull()
                                )
                    )
                )
            )
        )
    }
    val dataSet = PDataSet(
        dataList.toList(),
        attrNames.mapIndexed { i, name -> PDataSet.PAttribute(name, attrVals[i]) },
        PDataSet.PAttribute("class", classVals)
    )
    dataSet.attrs.forEach{
        println("Gain ratio and unique attrs for ${it.name}: ${dataSet.gainRatio(dataSet.targetVar, it)}, ${it.values.size}")
        //print(it.values.size.toString() + " ")
    }

}